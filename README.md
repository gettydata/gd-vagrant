Phoenix Vagrant
=========

Virtual Machine for GettyData

  - Normalize developers virtual machine
  - Using Virtualbox
  - Support RubyMine features

Installation
--------------

```sh
git clone git@github.com:gettydata/gd-vagrant.git
cd gd-vagrant
```

Start
-----

```sh
vagrant up --provider virtualbox
```

Stop
----

```sh
vagrant halt
```

Setting up Git
--------------

```ss
git config --global user.name "YOUR NAME"
git config --global user.email "YOUR EMAIL ADDRESS"
```

[Configuration SSH access](https://help.github.com/articles/generating-ssh-keys)
--------------------------

